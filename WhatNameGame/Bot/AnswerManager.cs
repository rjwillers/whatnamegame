﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhatNameGame.DataAccess.Repository;

namespace WhatNameGame.Bot
{
    public class AnswerManager : IAnswerManager
    {
        public const string ANIMAL_NAME = "animal_name";
        public const string FIRST_ANSWER = "first_answer";
        public const string SECOND_ANSWER = "second_answer";
        public const string THIRD_ANSWER = "third_answer";

        private IGamePreferenceManager _gamePreferenceManager;
        private IAnimalDescriptionRepository _animalDescriptionRepository;
        private IAnimalRepository _animalRepository;

        public AnswerManager(IGamePreferenceManager gamePreferenceManager, IAnimalDescriptionRepository animalDescriptionRepository, IAnimalRepository animalRepository)
        {
            _gamePreferenceManager = gamePreferenceManager;
            _animalDescriptionRepository = animalDescriptionRepository;
            _animalRepository = animalRepository;
        }

        private string AnimalName
        {
            get
            {
                return _gamePreferenceManager.GetStringPreferenceValue(ANIMAL_NAME);
            }
        }

        private string FirstAnswer
        {
            get
            {
                return _gamePreferenceManager.GetStringPreferenceValue(FIRST_ANSWER);
            }
        }

        private string SecondAnswer
        {
            get
            {
                return _gamePreferenceManager.GetStringPreferenceValue(SECOND_ANSWER);
            }
        }
        private string ThirdAnswer
        {
            get
            {
                return _gamePreferenceManager.GetStringPreferenceValue(THIRD_ANSWER);
            }   
        }

        public string ReturnTheActualAnswer()
        {
            return _gamePreferenceManager.GetStringPreferenceValue(ANIMAL_NAME);
        }

        public async Task<string> ProcessAnswerAsync()
        {
            var answer = string.Empty;

            var animalDescriptions = await _animalDescriptionRepository.GetAnimalDescriptionsAsync();
            var relatedDescriptions = (from ad in animalDescriptions
                                        where ad.DescriptionId == FirstAnswer ||
                                            ad.DescriptionId == SecondAnswer||
                                            ad.DescriptionId == ThirdAnswer
                                        group ad.DescriptionId by ad.AnimalId into groupedAnimalIdByDescription
                                        select new { AnimalId = groupedAnimalIdByDescription.Key, DescriptionIdList = groupedAnimalIdByDescription.ToList() }).ToList() ;

            if (relatedDescriptions.Count() > 0)
            {
                var animals = await _animalRepository.GetAnimalsAsync();

                if (relatedDescriptions.Count() == 1)
                {
                    var animal = (from a in animals
                                    where a.Id == relatedDescriptions.First().AnimalId
                                    select a).First();


                    answer = animal.Name;

                    return answer;
                }
                else
                {
                    var highestCount = 0;
                    var animalIdList = new List<string>();

                    var relatedDescriptionIndexLength = relatedDescriptions.Count();
                    for (var i = 0; i <= relatedDescriptions.Count() - 1; i++)
                    {
                        if (highestCount != 0 &&
                            relatedDescriptions[i].DescriptionIdList.Count() == highestCount)
                        {
                            animalIdList.Add(relatedDescriptions[i].AnimalId);
                        }
                        else if (relatedDescriptions[i].DescriptionIdList.Count > highestCount)
                        {
                            animalIdList.Clear();
                            animalIdList.Add(relatedDescriptions[i].AnimalId);
                            highestCount = relatedDescriptions[i].DescriptionIdList.Count;
                        }
                    }

                    var answerBuilder = new StringBuilder();
                    foreach(var id in animalIdList)
                    {
                        var animal = (from a in animals
                                        where a.Id == id
                                        select a).First();

                        answerBuilder.Append(animal.Name + " or ");

                    }

                    answer = answerBuilder.ToString().Remove(answerBuilder.ToString().Length - 4, 4);
                }
            }
            return answer;
        }

        public void StoreSelectedAnimal(string value)
        {
            _gamePreferenceManager.AddStringPreference(ANIMAL_NAME, value);
        }

        public void StoreSelectedDescription(string key, string value)
        {
            _gamePreferenceManager.AddStringPreference(key, value);
        }
    }
}