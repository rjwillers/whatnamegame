﻿using System.Threading.Tasks;

namespace WhatNameGame.Bot
{
    public interface IAnswerManager
    {
        void StoreSelectedAnimal(string value);
        void StoreSelectedDescription(string key, string value);
        Task<string> ProcessAnswerAsync();
        string ReturnTheActualAnswer();
    }
}