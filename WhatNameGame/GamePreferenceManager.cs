﻿
using Android.Content;
using System.Collections.Generic;

namespace WhatNameGame
{
    public class GamePreferenceManager : IGamePreferenceManager
    {       
        private Context GetTheAppContext()
        {
            return Android.App.Application.Context;
        }

        private ISharedPreferences _preferences;
        public ISharedPreferences Preferences
        {
            get
            {
                if(_preferences == null)
                {
                    _preferences = GetTheAppContext().GetSharedPreferences("WhatNameGame", FileCreationMode.Private);
                }

                return _preferences;
            }
        }

        private ISharedPreferencesEditor _preferencesEdit;
        private ISharedPreferencesEditor PreferencesEdit
        {
            get
            {
                if(_preferencesEdit == null)
                {
                    _preferencesEdit = GetTheAppContext().GetSharedPreferences("WhatNameGame", FileCreationMode.Private).Edit();
                }
                return _preferencesEdit;
            }
        }
        
        public void AddStringPreference(string key, string value)
        {
            PreferencesEdit.PutString(key, value);
            PreferencesEdit.Commit();
        }

        public void DeleteAllPreferences()
        {
            PreferencesEdit.Clear();
            PreferencesEdit.Commit();
        }

        public void DeletePreference(string key)
        {
            if (Preferences.Contains(key))
            {
                PreferencesEdit.Remove(key);
                PreferencesEdit.Commit();
            }
        }

        public IDictionary<string, object> GetAllSharedPreferences()
        {
            return Preferences.All;
        }

        public string GetStringPreferenceValue(string key)
        {
            var value = Preferences.GetString(key, "");
            return value;
        }

        public void UpdateStringPreference(string key, string value)
        {
            
        }
    }
}