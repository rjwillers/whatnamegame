﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Ninject;

namespace WhatNameGame
{
    [Activity(Label = "WhatNameGame", MainLauncher = true)]
    public class MainActivity : Activity
    {

        Button btnGuessWhat;
        Button btnManageGame;
        Button btnClose;
        IGamePreferenceManager _gamePreferenceManager;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);
            btnGuessWhat = (Button)FindViewById(Resource.Id.btnGuessWhat);
            btnGuessWhat.Click += BtnGuessWhat_Click;

            btnManageGame = (Button)FindViewById(Resource.Id.btnManageGame);
            btnManageGame.Click += BtnManageGame;


            btnClose = (Button)FindViewById(Resource.Id.btnClose);
            btnClose.Click += BtnClose_Click;

            _gamePreferenceManager = App.Container.Get<GamePreferenceManager>();
            _gamePreferenceManager.DeleteAllPreferences();
        }

        private void BtnClose_Click(object sender, System.EventArgs e)
        {
            this.FinishAffinity();
        }

        private void BtnGuessWhat_Click(object sender, System.EventArgs e)
        {

            var guessWhatActivity = new Intent(this, typeof(GuessWhatActivity));
            guessWhatActivity.AddFlags(ActivityFlags.NewTask);
            StartActivity(guessWhatActivity);
        }

        private void BtnManageGame(object sender, System.EventArgs e)
        {
            Toast.MakeText(this, "Available when hired =P", ToastLength.Short).Show();
        }
    }
}

