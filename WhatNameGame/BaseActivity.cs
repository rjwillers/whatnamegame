﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.WindowsAzure.MobileServices;
using Ninject;
using WhatNameGame.Bot;
using WhatNameGame.DataAccess;
using WhatNameGame.DataAccess.Repository;

namespace WhatNameGame
{
    [Activity(Label = "BaseActivity")]
    public class BaseActivity : ListActivity
    {
        public IMobileServiceClient _mobileServiceClient;
        public IAnimalRepository _animalRepository;
        public IAnimalDescriptionRepository _animalDescriptionRepository;
        public IDescriptionRepository _descriptionRepository;
        public IGamePreferenceManager _gamePreferenceManager;
        public IAnswerManager _answerManager;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _mobileServiceClient = App.Container.Get<IMobileServiceClient>("MobileServiceClient");
            _animalRepository = App.Container.Get<AnimalRepository>();
            _descriptionRepository = App.Container.Get<DescriptionRepository>();
            _animalDescriptionRepository = App.Container.Get<AnimalDescriptionRepository>();
            _gamePreferenceManager = App.Container.Get<GamePreferenceManager>();
            _answerManager = App.Container.Get<AnswerManager>();
            
        }
    }
}