﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using WhatNameGame.Bot;
using WhatNameGame.DataAccess.Adapters;
using WhatNameGame.DataAccess.Model;

namespace WhatNameGame
{
    [Activity(Label = "GuessingActivity")]
    public class GuessingActivity : BaseActivity
    {
        IList<Description> descriptions;
        ProgressDialog loadingDialog;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetTitle(Resource.String.what_describes_me);
            loadingDialog = new ProgressDialog(this);
            await DisplayAnimalListView();
        }

        protected async Task DisplayAnimalListView()
        {
            loadingDialog.SetMessage("Processing...");
            loadingDialog.Show();

            descriptions = await _descriptionRepository.GetDescriptionsAsync();

            var selectedItems = _gamePreferenceManager.GetAllSharedPreferences();

            foreach (var item in selectedItems)
            {
                var removeDescription = descriptions.FirstOrDefault(i => i.Id == item.Value.ToString());
                if (removeDescription != null)
                {
                    descriptions.Remove(removeDescription);
                }
            }

            ListAdapter = new DescriptionAdapter(this, descriptions);
            loadingDialog.Dismiss();
        }
        
        protected override async void OnListItemClick(ListView l, View v, int position, long id)
        {
            var description = descriptions[position];
            Toast.MakeText(this, description.Value, ToastLength.Short).Show();
            
            //Check if 1st and 2nd answers captured with the 3rd answer pending
            if (string.IsNullOrEmpty(_gamePreferenceManager.GetStringPreferenceValue(AnswerManager.THIRD_ANSWER)) &&
                !string.IsNullOrEmpty(_gamePreferenceManager.GetStringPreferenceValue(AnswerManager.FIRST_ANSWER)) &&
                !string.IsNullOrEmpty(_gamePreferenceManager.GetStringPreferenceValue(AnswerManager.SECOND_ANSWER)))
            {
                _gamePreferenceManager.AddStringPreference(AnswerManager.THIRD_ANSWER, description.Id);

                loadingDialog.SetMessage("Hmmm let me think...");
                loadingDialog.Show();
                var answer = await _answerManager.ProcessAnswerAsync();
                loadingDialog.Dismiss();

                Toast.MakeText(this, string.Format("The animal you chose is {0}", answer), ToastLength.Short).Show();
            }
            else
            {
                //Check if 1st answer captured and 2nd is pending
                if (string.IsNullOrEmpty(_gamePreferenceManager.GetStringPreferenceValue(AnswerManager.SECOND_ANSWER)) &&
                    !string.IsNullOrEmpty(_gamePreferenceManager.GetStringPreferenceValue(AnswerManager.FIRST_ANSWER)))
                {
                    _gamePreferenceManager.AddStringPreference(AnswerManager.SECOND_ANSWER, description.Id);
                }

                //check is  1st answer still pending
                if (string.IsNullOrEmpty(_gamePreferenceManager.GetStringPreferenceValue(AnswerManager.FIRST_ANSWER)))
                {
                    _gamePreferenceManager.AddStringPreference(AnswerManager.FIRST_ANSWER, description.Id);
                }

                var guessingActivity = new Intent(this, typeof(GuessingActivity));
                guessingActivity.AddFlags(ActivityFlags.ClearTop);
                guessingActivity.AddFlags(ActivityFlags.ClearWhenTaskReset);
                StartActivity(guessingActivity);
                Finish();
            }
        }
    }
}