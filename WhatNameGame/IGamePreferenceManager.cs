﻿using Android.Content;
using System.Collections.Generic;

namespace WhatNameGame
{
    public interface IGamePreferenceManager
    {
        IDictionary<string, object> GetAllSharedPreferences();
        void DeleteAllPreferences();
        void AddStringPreference(string key, string value);
        void UpdateStringPreference(string key, string value);
        void DeletePreference(string key);
        string GetStringPreferenceValue(string key);
    }
}