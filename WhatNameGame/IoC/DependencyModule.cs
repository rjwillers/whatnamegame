﻿using Microsoft.WindowsAzure.MobileServices;
using Ninject.Modules;
using WhatNameGame.Bot;
using WhatNameGame.DataAccess;
using WhatNameGame.DataAccess.Repository;

namespace WhatNameGame.IoC
{
    public class DependencyModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMobileServiceClient>().ToConstructor(c => new MobileServiceClient("https://website20180228073355.azurewebsites.net")).Named("MobileServiceClient");
            Bind<IAnimalRepository>().ToConstructor(c => new AnimalRepository(c.Inject<MobileServiceClient>())).InSingletonScope();
            Bind<IDescriptionRepository>().ToConstructor(c => new DescriptionRepository(c.Inject<MobileServiceClient>())).InSingletonScope();
            Bind<IAnimalDescriptionRepository>().ToConstructor(c => new AnimalDescriptionRepository(c.Inject<MobileServiceClient>())).InSingletonScope();
            
            Bind<IGamePreferenceManager>().To<GamePreferenceManager>();
            Bind<IAnswerManager>().ToConstructor(c => new AnswerManager(c.Inject<IGamePreferenceManager>(), c.Inject<IAnimalDescriptionRepository>(), c.Inject<IAnimalRepository>()));
        }
    }
}