﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Widget;
using WhatNameGame.DataAccess.Model;

namespace WhatNameGame.DataAccess.Adapters
{
    public class DescriptionAdapter : BaseAdapter<Description>
    {
        Activity context = null;
        IList<Description> descriptions = new List<Description>();

        public DescriptionAdapter(Activity context, IList<Description> descriptions) : base()
        {
            this.context = context;
            this.descriptions = descriptions;
        }

        public override Description this[int position]
        {
            get { return descriptions[position]; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override int Count
        {
            get { return descriptions.Count; }
        }

        public override Android.Views.View GetView(int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
        {
            var item = descriptions[position];

            var view = (convertView ?? context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem1, parent, false)) as TextView;
            view.SetText(item.Value, TextView.BufferType.Normal);
            
            return view;
        }
    }
}