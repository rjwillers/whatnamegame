﻿using Microsoft.WindowsAzure.MobileServices;
using System.Collections.Generic;
using System.Threading.Tasks;
using WhatNameGame.DataAccess.Model;

namespace WhatNameGame.DataAccess.Repository
{
    public class AnimalDescriptionRepository  : IAnimalDescriptionRepository
    {

        IMobileServiceClient _mobileServiceClient;
        public AnimalDescriptionRepository(IMobileServiceClient mobileServiceClient)
        {
            _mobileServiceClient = mobileServiceClient;
        }

        private IMobileServiceTable<AnimalDescription> _animalTableDescription;
        public IMobileServiceTable<AnimalDescription> AnimalDescriptionTable
        {
            get
            {
                if (_animalTableDescription == null)
                {
                    //_animalTableDescription = new DataService().Instance.Service.GetTable<AnimalDescription>();
                    _animalTableDescription = _mobileServiceClient.GetTable<AnimalDescription>();
                }

                return _animalTableDescription;
            }
        }

        public async Task InsertAnimalDescriptionAsync(AnimalDescription animal)
        {
            await AnimalDescriptionTable.InsertAsync(animal);
        }

        public async Task<List<AnimalDescription>> GetAnimalDescriptionsAsync()
        {
            List<AnimalDescription> animalDescriptions = await AnimalDescriptionTable.ToListAsync();
            return animalDescriptions;
        }
    }
}