﻿using Microsoft.WindowsAzure.MobileServices;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using WhatNameGame.DataAccess.Model;
using WhatNameGame.DataAccess.Repository;

namespace WhatNameGame.DataAccess
{
    public class DescriptionRepository : IDescriptionRepository
    {
        IMobileServiceClient _mobileServiceClient;
        public DescriptionRepository(IMobileServiceClient mobileServiceClient)
        {
            _mobileServiceClient = mobileServiceClient;
        }

        private IMobileServiceTable<Description> _descriptionTable;
        public IMobileServiceTable<Description> DescriptionTable
        {
            get
            {
                if (_descriptionTable == null)
                {
                    //_descriptionTable = new DataService().Instance.Service.GetTable<Description>();
                    _descriptionTable = _mobileServiceClient.GetTable<Description>();
                }

                return _descriptionTable;
            }
        }

        public async Task InsertDescriptionAsync(Description description)
        {
            await DescriptionTable.InsertAsync(description);
        }

        public async Task<List<Description>> GetDescriptionsAsync()
        {
            List<Description> descriptions = await DescriptionTable
              .OrderBy(item => item.Value)
              .ToListAsync();

            return new List<Description>(descriptions);
        }
    }
}