﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using WhatNameGame.DataAccess.Model;
using WhatNameGame.DataAccess.Repository;

namespace WhatNameGame.DataAccess
{
    public class AnimalRepository : IAnimalRepository
    {
        IMobileServiceClient _mobileServiceClient;
        public AnimalRepository(IMobileServiceClient mobileServiceClient)
        {
            _mobileServiceClient = mobileServiceClient;
        }

        private IMobileServiceTable<Animal> _animalTable;
        public IMobileServiceTable<Animal> AnimalTable
        {
            get
            {
                if(_animalTable == null)
                {
                    //_animalTable = new DataService().Instance.Service.GetTable<Animal>();
                    _animalTable = _mobileServiceClient.GetTable<Animal>();
                }

                return _animalTable;
            }
        }

        public async Task InsertAnimalAsync(Animal animal)
        {
            await AnimalTable.InsertAsync(animal);
        }

        public async Task<List<Animal>> GetAnimalsAsync()
        {
            List<Animal> animals = await AnimalTable
              .OrderBy(item => item.Name)
              .ToListAsync();

            return animals;
        }
    }

}