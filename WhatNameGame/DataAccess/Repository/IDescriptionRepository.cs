﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using WhatNameGame.DataAccess.Model;

namespace WhatNameGame.DataAccess.Repository
{
    public interface IDescriptionRepository
    {
        Task InsertDescriptionAsync(Description description);
        Task<List<Description>> GetDescriptionsAsync();
    }
}