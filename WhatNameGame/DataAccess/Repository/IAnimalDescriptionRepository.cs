﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WhatNameGame.DataAccess.Model;

namespace WhatNameGame.DataAccess.Repository
{
    public interface IAnimalDescriptionRepository
    {
        Task InsertAnimalDescriptionAsync(AnimalDescription animal);
        Task<List<AnimalDescription>> GetAnimalDescriptionsAsync();
    }
}