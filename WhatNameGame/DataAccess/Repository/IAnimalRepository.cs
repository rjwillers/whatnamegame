﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WhatNameGame.DataAccess.Model;

namespace WhatNameGame.DataAccess.Repository
{
    public interface IAnimalRepository
    {
        Task InsertAnimalAsync(Animal animal);
        Task<List<Animal>> GetAnimalsAsync();
    }
}