﻿using Newtonsoft.Json;

namespace WhatNameGame.DataAccess.Model
{
    public class Animal
    {
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}