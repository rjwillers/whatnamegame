﻿using Newtonsoft.Json;

namespace WhatNameGame.DataAccess.Model
{
    public class AnimalDescription
    {
        public string Id { get; set; }

        [JsonProperty(PropertyName = "animalId")]
        public string AnimalId { get; set; } 

        [JsonProperty(PropertyName = "descriptionId")]
        public string DescriptionId { get; set; }
    }
}