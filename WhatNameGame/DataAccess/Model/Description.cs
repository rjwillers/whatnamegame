﻿using Newtonsoft.Json;

namespace WhatNameGame.DataAccess.Model
{
    public class Description
    {
        public string Id { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }
}