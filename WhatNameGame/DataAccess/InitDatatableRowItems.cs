﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using WhatNameGame.DataAccess.Model;
using WhatNameGame.DataAccess.Repository;

namespace WhatNameGame.DataAccess
{
    public static class InitDatatableRowItems
    {
        public static async void InsertAnimals()
        {
            var lion = new Animal() { Name = "Lion" };
            var elephant = new Animal() { Name = "Elephant" };
            //await AnimalRepository.InsertAnimalAsync(elephant);
            //await AnimalRepository.InsertAnimalAsync(lion);
        }

        public static async void InsertDescriptions()
        {
            var one = new Description() { Value = "I have a trunk." };
            var two = new Description() { Value = "I'am grey in colour." };
            var three = new Description() { Value = "I trumpet." };
            var four = new Description() { Value = "I have a mane." };
            var five = new Description() { Value = "I roar." };
            var six = new Description() { Value = "I'am yellow." };

            //await DescriptionRepository.InsertDescriptionAsync(one);
            //await DescriptionRepository.InsertDescriptionAsync(two);
            //await DescriptionRepository.InsertDescriptionAsync(three);
            //await DescriptionRepository.InsertDescriptionAsync(four);
            //await DescriptionRepository.InsertDescriptionAsync(five);
            //await DescriptionRepository.InsertDescriptionAsync(six);
        }

        public static async void InsertAnimalDescriptions()
        {
            var one = new AnimalDescription() { AnimalId = "f47067f4-e97e-421f-8498-df17c5942c69", DescriptionId = "e7d59673-0174-4292-8d62-56c30e747db2" };
            var two = new AnimalDescription() { AnimalId = "f47067f4-e97e-421f-8498-df17c5942c69", DescriptionId = "2191e441-913a-4a43-bb84-2de495fca713" };
            var three = new AnimalDescription() { AnimalId = "f47067f4-e97e-421f-8498-df17c5942c69", DescriptionId = "327dc5e5-ae4f-4664-82a1-5654b48916c8" };

            var four = new AnimalDescription() { AnimalId = "237d79cb-82b4-46dc-990a-d292108cb343", DescriptionId = "e8399c68-723f-454d-ab5a-45aa23b83f80" };
            var five = new AnimalDescription() { AnimalId = "237d79cb-82b4-46dc-990a-d292108cb343", DescriptionId = "93b1ebce-e882-442d-8365-ee0b1ec285a1" };
            var six = new AnimalDescription() { AnimalId = "237d79cb-82b4-46dc-990a-d292108cb343", DescriptionId = "55974ecb-767f-4162-b73b-7e5a5be81c0c" };

            //var animalDescriptions = new AnimalDescriptionRepository();
            //await animalDescriptions.InsertAnimalDescriptionAsync(one);
            //await animalDescriptions.InsertAnimalDescriptionAsync(two);
            //await animalDescriptions.InsertAnimalDescriptionAsync(three);
            //await animalDescriptions.InsertAnimalDescriptionAsync(four);
            //await animalDescriptions.InsertAnimalDescriptionAsync(five);
            //await animalDescriptions.InsertAnimalDescriptionAsync(six);
        }
    }
}