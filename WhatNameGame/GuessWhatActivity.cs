﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using WhatNameGame.Bot;

namespace WhatNameGame
{
    [Activity(Label = "GuessWhatActivity")]
    public class GuessWhatActivity : BaseActivity
    {
        string[] _animalNames;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetTitle(Resource.String.guess_what_title);
            await DisplayAnimalListView();
        }

        protected async Task DisplayAnimalListView()
        {
            var loadingDialog = new ProgressDialog(this);
            loadingDialog.SetMessage("Retrieving animals...");
            loadingDialog.Show();

            var animals = await _animalRepository.GetAnimalsAsync();
            _animalNames = animals.Select(i => i.Name).ToArray();
            ListAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, _animalNames);

            loadingDialog.Dismiss();
        }

        protected override void OnListItemClick(ListView l, View v, int position, long id)
        {
            var animalName = _animalNames[position];
            _gamePreferenceManager.AddStringPreference(AnswerManager.ANIMAL_NAME, animalName);
                
            Toast.MakeText(this, animalName, ToastLength.Short).Show();
            var guessingActivity = new Intent(this, typeof(GuessingActivity));

            StartActivity(guessingActivity);
        }
    }
}